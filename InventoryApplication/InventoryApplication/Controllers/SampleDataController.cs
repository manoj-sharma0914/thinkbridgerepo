using InventoryApplication.Model;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryApplication.Controllers
{
    [Route("api/[controller]")]
    public class SampleDataController : Controller
    {
        private readonly DataContext _dataContext;
        private readonly IHostingEnvironment _hostingEnvironment;
        public SampleDataController(DataContext dataContext, IHostingEnvironment hostingEnvironment)
        {
            _dataContext = dataContext;
            _hostingEnvironment = hostingEnvironment;
        }

        /// <summary>
        /// this function returns the List of items
        /// </summary>
        /// <returns></returns>
        [HttpGet("getAll")]
        public string Inventories()
        {
            return JsonConvert.SerializeObject(_dataContext.Inventory.ToList());
        }
        /// <summary>
        /// This function add the item in Database
        /// </summary>
        /// <param name="inventoryModel"></param>
        /// <returns></returns>
        [HttpPost("Create")]
        public IActionResult Create([FromBody] InventoryModel inventoryModel)
        {
            string[] splitValue = inventoryModel.ImageUrl.Split('\\');
            InventoryModel inventory = new InventoryModel()
            {
                Name = inventoryModel.Name,
                Description = inventoryModel.Description,
                Price = inventoryModel.Price,
                ImageUrl = splitValue[2]
            };

            _dataContext.Inventory.Add(inventory);
            _dataContext.SaveChanges();

            return Ok();
        }

        /// <summary>
        /// This function Update the item in the Database.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] InventoryModel obj)
        {
            var data = _dataContext.Inventory.Update(obj);
            _dataContext.SaveChanges();
            return Ok();
        }

        /// <summary>
        /// This function Delete the Item from database.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var data = _dataContext.Inventory.Where(a => a.ItemId == id).FirstOrDefault();
            _dataContext.Inventory.Remove(data);
            _dataContext.SaveChanges();
            return Ok();

        }

        /// <summary>
        /// this Function returns the item as per itemId from database.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public string GetItemById(int id)
        {
            return JsonConvert.SerializeObject(_dataContext.Inventory.Where(a => a.ItemId == id).FirstOrDefault());
        }

        /// <summary>
        /// this function upload the image in the specific folder.
        /// </summary>
        /// <returns></returns>
        [HttpPost("UploadImage")]
        public async Task<string> UploadImage()
        {
            try
            {
                var imageurl = Request.Form.Files[0];
                string[] fileExtensionsArray = imageurl.FileName.Split('.').ToArray();
                string fileExtension = fileExtensionsArray[fileExtensionsArray.Length - 1];

                //Gets the root directory path
                string uploads = Path.Combine(_hostingEnvironment.ContentRootPath + "\\ClientApp\\Src\\assets\\", "Uploads");

                string filename = GetUniqueFileName(imageurl.FileName);
                //Combine the root directory path and image name
                string filepath = Path.Combine(uploads, filename);

                // Copies the file to root directory
                using (Stream stream = new FileStream(filepath, FileMode.Create))
                {
                    await imageurl.CopyToAsync(stream);
                }

                return imageurl.FileName;

            }
            catch (System.Exception e)
            {

                throw;
            }

        }

        /// <summary>
        /// this function generates the unique name for the image.
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private string GetUniqueFileName(string fileName)
        {

            return Guid.NewGuid().ToString("N").Substring(0, 8) + Path.GetExtension(fileName);
        }

    }
}
