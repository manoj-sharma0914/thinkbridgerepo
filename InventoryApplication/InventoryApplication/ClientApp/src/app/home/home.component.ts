import { Component,Inject, OnInit } from '@angular/core';
import { FormGroup,FormControl,Validators, FormBuilder } from '@angular/forms';
import { InventoryService } from '../service/inventory.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit   {
  public inventories: Inventories[];
  data:any[];
  form: FormGroup;
  imageUrl:any;
  constructor(private formBuilder: FormBuilder, private inventoryService:InventoryService) {}
  ngOnInit(){
  this.getdata();
  this.form = this.formBuilder.group  ({
  Name:[''],
  Description:[''],
  Price:[''],
  ImageUrl:['']
})
}


  getdata() {  
    this.inventoryService.getAll().subscribe((result:any)=> {  
     this.inventories = result;  
    }) 
  }


  SaveInventory(){
    console.log(JSON.stringify(this.form.value));
        this.inventoryService.create(this.form.value).subscribe(result=>{
      this.getdata();
      this.resetForm();
    },error=>console.log(error))
        }

  UploadFile(event){
   
    if (event.target.files.length > 0) {
      const file =event.target.files[0];
      var formdata = new FormData();
      formdata.append('files',file,event.target.files[0].name);
      var imageName = this.inventoryService.uploadImage(formdata).subscribe(result=>{
        this.form.get('ImageUrl').setValue(imageName);
        }, error=>console.log(error))
     
    }
  }

  deleteInventory(id){
   
    this.inventoryService.delete(id).subscribe(result=>{
      this.getdata();
          }, error=>console.log(error))
  }

 
  resetForm(){
    this.form.reset();
  }
}

export interface Inventories {
  ItemId:number;
  Name: string;
  Description: string;
  Price: number;
  ImageUrl: any;
}