import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { InventoryService } from '../service/inventory.service';
import { FormGroup,FormBuilder } from '@angular/forms';



@Component({
  selector: 'app-fetch-data',
  templateUrl: './fetch-data.component.html'
})
export class FetchDataComponent implements OnInit{
 form: FormGroup;
  itemid:any;
  ImageUrl:string;
  constructor(
    private inventoryService:InventoryService, 
    private route:ActivatedRoute,
    private formBuilder: FormBuilder,
    private router:Router
    ) {}
    
    
  ngOnInit(){
    this.form = this.formBuilder.group  ({
      ItemId:[''],
      Name:[''],
      Description:[''],
      Price:[''],
      ImageUrl:['']
    });
    this.route.paramMap.subscribe(params=>{
     this.itemid = params.get('ItemId');
      })
      this.getItemById(this.itemid);
  }
  getItemById(id){
    this.inventoryService.getItemById(id).subscribe((result:any)=> { 
      this.form.get("ItemId").setValue(<number>result.ItemId);
      this.form.get("Name").setValue(result.Name);
      this.form.get("Description").setValue(result.Description);
      this.form.get("Price").setValue(result.Price);
      this.form.get("ImageUrl").setValue(result.ImageUrl);
      this.ImageUrl= result.ImageUrl;
    }) 
  }
  
  UpdateInventory(){
    console.log(this.form.value);
    this.inventoryService.update(this.form.value).subscribe((result:any)=>{
      this.router.navigate([''])
    })
   
   
  }
}


