import { BadInput } from '../common/bad-input';
import { NotFoundError } from '../common/not-found-error';
import { AppError } from '../common/app-error';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import {map,catchError} from 'rxjs/operators'



@Injectable()
export class DataserviceService {
  constructor(@Inject(String) private url, private http: HttpClient) { }
  Myhead = new HttpHeaders().set('Content-Type', 'application/json'); 
    
  getAll() {
    return this.http.get(this.url+"getAll")
      .pipe(map(response => response),
      catchError(this.handleError));
  }

  getItemById(id){
    return this.http.get(this.url +"/"+id)
    .pipe(map(response => response),
    catchError(this.handleError));
  }
  create(resource){
    return this.http.post(this.url+"Create", resource,{headers: this.Myhead})
    .pipe(map(response => response),
    catchError(this.handleError));
  }

  update(resource) {
    return this.http.put(this.url + '/' + resource.id, resource)
    .pipe(map(response => response),
    catchError(this.handleError));
  }

  delete(id) {
    return this.http.delete(this.url + '/' + id)
    .pipe(map(response => response),
    catchError(this.handleError));
  }

  uploadImage(image){
    return this.http.post(this.url+"UploadImage",image,)
    .pipe(map(response => response),
    catchError(this.handleError));
  }
  private handleError(error: Response) {
    console.log(error);
    if (error.status === 400)
      return Observable.throw(new BadInput(error.json()));
  
    if (error.status === 404)
      return Observable.throw(new NotFoundError());
    
    return Observable.throw(new AppError(error));
  }
}
