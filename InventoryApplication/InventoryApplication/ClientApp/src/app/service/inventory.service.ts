import { Injectable } from '@angular/core';
import { DataserviceService } from './dataservice.service';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class InventoryService extends DataserviceService{

  constructor(http:HttpClient) { 
    super('http://localhost:53644/api/SampleData/', http);
  }
}
