﻿using System.ComponentModel.DataAnnotations;

namespace InventoryApplication.Model
{
    public class InventoryModel
    {
        [Key]
        public int ItemId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Price { get; set; }
        public string ImageUrl { get; set; }
    }
}
