﻿using Microsoft.EntityFrameworkCore;

namespace InventoryApplication.Model
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {

        }
        public DbSet<InventoryModel> Inventory { get; set; }
    }
}
